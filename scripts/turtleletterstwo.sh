#!/bin/bash

rosservice call /turtle1/set_pen 200 120 80 4 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,0.0,0.0]' '[0.0,0.0,2.6]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5.0,0.0,0.0]' '[0.0,0.0,4.3]'

rosservice call /turtle1/set_pen 200 120 80 4 1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,0.0,0.0]' '[0.0,0.0,0.6]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[3.5,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,0.0,0.0]' '[0.0,0.0,-2.8]'

rosservice call /turtle1/set_pen 180 60 210 8 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[3.5,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,0.0,0.0]' '[0.0,0.0,3.1]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.0,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.0,0.0,0.0]' '[0.0,0.0,-3.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.2,0.0,0.0]' '[0.0,0.0,0.0]'
