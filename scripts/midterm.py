import rospy, sys
import moveit_commander
import copy
from math import pi

class MoveIt_my_initials:
    # initialization function
    def __init__(self):
        
        # initialize moveit_commander and a rospy node
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('moveit_my_initial', anonymous=True)
        
        # instantiate a MoveGroupCommander object
        self.group = moveit_commander.MoveGroupCommander('manipulator')
        
        # set starting joint positions
        joint_positions = [0, -pi/2, 1, 0, 0, 0]
        self.group.set_joint_value_target(joint_positions)
        self.group.go()
        rospy.sleep(1)
    
    # executable function for planning initial paths
    def plan_initial_paths(self, scale=1):

        # Cartesian path of letter C
        waypoints = []  
        
        wpose = self.group.get_current_pose().pose   
        wpose.position.y -= scale * 0.25
        waypoints.append(copy.deepcopy(wpose))   
        
        wpose.position.z -= scale * 0.25
        waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.y += scale * 0.25
        waypoints.append(copy.deepcopy(wpose))
        
        (plan, fraction) = self.group.compute_cartesian_path(
            waypoints, # waypoints to follow
            0.01,      # eef_step
            0.0        # jump_threshold
        )  
        
        # execute the path that has been planned
        self.group.execute(plan, wait=True)
        
        # Cartesian path of letter Y
        joint_positions = [0, -pi/2, 1, 0, 0, 0]
        self.group.set_joint_value_target(joint_positions)
        self.group.go()
        rospy.sleep(1)
        
        waypoints = []
        
        wpose = self.group.get_current_pose().pose   
        wpose.position.y += scale * 0.08
        wpose.position.z -= scale * 0.12
        waypoints.append(copy.deepcopy(wpose))   
        
        wpose.position.y += scale * 0.08
        wpose.position.z += scale * 0.12
        waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.y -= scale * 0.08
        wpose.position.z -= scale * 0.12
        waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.z -= scale * 0.15
        waypoints.append(copy.deepcopy(wpose))
        
        (plan, fraction) = self.group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        
        # execute the path
        self.group.execute(plan, wait=True)
        
        # Cartesian path of letter U
        # change joint starting positions to avoid singularity
        joint_positions = [0, -1.5, 1.2, 0.3, 0, 0]
        self.group.set_joint_value_target(joint_positions)
        self.group.go()
        rospy.sleep(1)
        
        waypoints = []
        
        wpose = self.group.get_current_pose().pose   
        wpose.position.z -= scale * 0.25
        waypoints.append(copy.deepcopy(wpose))   
        
        wpose.position.y += scale * 0.2
        waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.z += scale * 0.25
        waypoints.append(copy.deepcopy(wpose))
        
        (plan, fraction) = self.group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        
        # execute the path
        self.group.execute(plan, wait=True)
        
        
if __name__ == "__main__":
    my_initials = MoveIt_my_initials()
    try:
        my_initials.plan_initial_paths()
    except rospy.ROSInterruptException:
        pass
